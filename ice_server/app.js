'use strict';

var express = require('express'),
    app = express();
var dir = require('node-dir'),
    path = require('path');

require('config')(app);

function walk(dirname, callback){
  dir.files(dirname, function(err, files){
    if(err) throw err;

    files.forEach(callback);
  });
};
function relative_path(absolute_path){
  return absolute_path.replace(__dirname, '').replace(/^\//, '');
}

walk(__dirname + '/routes', function(filename){
  var module_fullname = relative_path(filename).replace(path.extname(filename), '');
  var module_name = path.basename(module_fullname);

  var router = require(module_fullname);
  app.use('/' + (module_name == 'index' ? '' : module_name), router);
});

module.exports = exports = app;
