FROM ubuntu:14.04

ENV HOME /root
WORKDIR /root

RUN \
  sed -i 's/# \(.*multiverse$\)/\1/g' /etc/apt/sources.list && \
  apt-get update && \
  apt-get -y upgrade && \
  curl -sL https://deb.nodesource.com/setup | bash - && \
  apt-get install -y build-essential nodejs nginx && \
  service nginx start

EXPOSE 80

ENTRYPOINT ["/usr/bin/nodejs"]
