'use strict';

var app = require('../app'),
    router = require('express').Router();

router.get('/', function(req, res){
  res.send('Hello!');
});

module.exports = exports = router;
